[1.4.0] - 2-4-2024
======================
#### Updated
 - Updated rules and rules command structs to add new fields:
   * repairOnReset - Whether or not to repair damage when resetting boat.
   * enableDamage - Whether or not to enable damage.
   

[1.3.0] - 12-13-2024
======================
#### Updated
 - Updated server settings struct to support new fields:
   * publicAddress
   * spectatorPassword
   * allowedBoatClasses
   * spectatorPasswordRequired

#### Fixed
 - Fixed a bug where chat_messages would be populated by
   multiple instances of HydroSimSDK.


[1.2.0] - 9-5-2024
======================
#### Updated
 - Updated the rules struct to support new Score-Up rules.


[1.1.2] - 8-26-2024
======================
#### Fixed
 - Removed a print when sending commands to HydroSim.


[1.1.1] - 8-24-2024
======================
#### Added
 - Added reset_buoys command (HydroSim v0.8.6.2)


[1.1.0] - 7-17-2024
======================
#### Updated
 - Updated support for HydroSim API 1.1.0 (HydroSim v0.8.6.0)

#### Added
 - Following fields were added to Telemetry:
   * rotationQuaternion
   * angularAcceleration
   * localAcceleration
   * localAngularAcceleration
   * maxRPM
   * limiterRPM
   * engineTorque
   * engineMaxTorque
   * engineIgnition
   * engineRunning
   * isTouchingWater
   * isTurbine
   * maxN2RPM
   * impactsCount
   * impacts