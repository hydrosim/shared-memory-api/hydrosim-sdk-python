"""
This module configures the package for distribution and installation.
"""

import json
from setuptools import setup, find_packages

with open("README.md") as f:
    readme = f.read()

with open("version.json") as fp:
    VERSION_INFO = json.load(fp)

setup(
    name="hydrosim_sdk",
    version=VERSION_INFO["version"],
    packages=find_packages(),
    description="HydroSim Shared Memory SDK",
    short_description="HydroSim Shared Memory SDK",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/hydrosim/shared-memory-api/hydrosim-sdk-python",
    install_requires=["dtypes", "pywin32; os_name=='nt'"],
    python_requires=">=3.9",
    license="MIT",
    classifiers=[
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: POSIX :: Linux",
    ],
)
